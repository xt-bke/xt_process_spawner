import time
import ConsumerProcess
import sys

def ProcessingFunction(ConsumerID, hdr, addData, payload):
	#print("PROCESSING DATA by Consumer " + str(ConsumerID))
	#print(hdr)
	return payload

args = sys.argv[1:]

print("Spawning process")
cp = ConsumerProcess.ConsumerProcess(True, ProcessingFunction, args)
cp.StartProcess()

while True:
	time.sleep(100)
