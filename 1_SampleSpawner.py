import time
import ProcessSpawner


def ProcessedData(hdr, addData, payload):
	print("PROCESSED DATA GET:")
	print(hdr)
	print(addData)
	print(payload)

for i in range(20):
	name = "cs" + str(i)
	cs = ProcessSpawner.ProcessSpawner("C:\\Arbeit\\AIXEMTEC\\DynPushPull\\ConsumerSpawnerProcessor\\1_SampleProcess.py", "127.0.0.1", 1234, "127.0.0.1", "2345", name, ProcessedData)
	cs.LocalSpawn()

addData = {
	"Testfield": "SomeText",
	"Param1": 5,
	"Param2": 12
}

while True:
	print("Sending Work")
	ProcessSpawner.SendWork("ImageProcessing", "Camera1", addData, b"abcdefg")
	time.sleep(0.1)