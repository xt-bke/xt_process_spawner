import time
import subprocess
import zmq
import sys
from threading import Thread
import json

class ConsumerProcess(object):
	def __init__(self, LocalProcess, WorkProcessorCB, args):
		self.SpawnSocket = None
		self.PullSocket = None
		self.PushSocket = None
		self.Arguments = args
		self.LocalProcess = LocalProcess
		self.ZMQContext = zmq.Context()
		self.SpawnSocketThread = None
		self.WorkerThread = None
		self.SpawnThreadRunning = False
		self.WorkerThreadRunning = False
		self.ConsumerID = 0
		self.WorkProcessorCB = WorkProcessorCB
		self.SpawnSocket = None


	def SpawnSocketMain(self):
		self.SpawnThreadRunning = True
		
		self.SpawnSocket = self.ZMQContext.socket(zmq.PAIR)
		if self.LocalProcess == True:
			self.SpawnSocket.connect("tcp://127.0.0.1:5655")
			self.ConsumerID = self.Arguments[4]
		else:
			self.SpawnSocket.bind("tcp://127.0.0.1:5655")

		while self.SpawnThreadRunning:
			# Receive Commands from producer
			msg = self.SpawnSocket.recv_json()
			print(msg)
			if "Req" in msg:
				if msg["Req"] == "spawn":
					ProducerAddr = msg["ProducerAddr"]
					ProducerPort = msg["ProducerPort"]
					ResultCollectorAddr = msg["ResultCollectorAddr"]
					ResultCollectorPort = msg["ResultCollectorPort"]
					self.PullSocket = self.ZMQContext.socket(zmq.PULL)
					self.PushSocket = self.ZMQContext.socket(zmq.PUSH)
					self.PullSocket.connect("tcp://"+ProducerAddr+":"+str(ProducerPort))
					self.PushSocket.connect("tcp://"+ResultCollectorAddr+":"+str(ResultCollectorPort))	
					self.ConsumerID = msg["ConsumerID"]
					self.WorkerThread.start()			

				if msg["Req"] == "shutdown":
					if "ConsumerID" in msg:
						if msg["ConsumerID"] == self.ConsumerID:
							self.SpawnThreadRunning = False
							self.WorkerThreadRunning = False
					else:
						self.SpawnThreadRunning = False
						self.WorkerThreadRunning = False

	def WorkerMain(self):
		self.WorkerThreadRunning = True
		
		if self.LocalProcess:
			self.PullSocket = self.ZMQContext.socket(zmq.PULL)
			self.PushSocket = self.ZMQContext.socket(zmq.PUSH)
			self.PullSocket.connect("tcp://"+self.Arguments[0]+":"+self.Arguments[1])
			self.PushSocket.connect("tcp://"+self.Arguments[2]+":"+self.Arguments[3])

		while self.WorkerThreadRunning:
			# Get data from producer
			workMessage = self.PullSocket.recv_multipart()
			
			# Get Header information
			hdr = workMessage[0].decode()
			hdr = json.loads(hdr)

			# Get Additional information
			addData = workMessage[1].decode()
			addData = json.loads(addData)

			# Get payload
			payload = workMessage[2]
			
			# Process
			processedData = self.WorkProcessorCB(self.ConsumerID, hdr, addData, payload)
			hdr["ConsumerID"] = self.ConsumerID			

			# Send to Result collector
			byteHdr = bytearray()
			byteHdr.extend(json.dumps(hdr).encode())
			byteAddData = bytearray()
			byteAddData.extend(json.dumps(addData).encode())
			bytePayload = bytearray()
			bytePayload.extend(processedData)
			self.PushSocket.send_multipart([byteHdr, byteAddData, bytePayload])
		
		self.PullSocket.close()
		self.PushSocket.close()

	def StartProcess(self):
		self.SpawnSocketThread = Thread(target=self.SpawnSocketMain, args=())
		self.SpawnSocketThread.daemon = True
		self.WorkerThread = Thread(target=self.WorkerMain, args=())
		self.WorkerThread.daemon = True	
		self.SpawnSocketThread.start()
		if self.LocalProcess == True:
			self.WorkerThread.start()