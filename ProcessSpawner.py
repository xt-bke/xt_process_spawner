import time
import subprocess
import zmq
from threading import Thread
import json

PushSocket = None
LocalSpawnerSocket = None
WorkerThreadStarted = False

def SendWork(WorkType, Tag, AddData, Payload):
	global PushSocket
	hdr = {"WorkType": WorkType, "Tag": Tag}

	byteHdr = bytearray()
	byteHdr.extend(json.dumps(hdr).encode())
	byteAddData = bytearray()
	byteAddData.extend(json.dumps(AddData).encode())
	bytePayload = bytearray()
	bytePayload.extend(Payload)
	PushSocket.send_multipart([byteHdr, byteAddData, bytePayload])

class ProcessSpawner(object):
	def __init__(self, ProcessPath, ProducerAddr, ProducerPort, ResultCollectorAddr, ResultCollectorPort, ConsumerID, ResultCollectorCB):
		global PushSocket
		
		self.ProcessPath = ProcessPath
		self.ProducerAddr = ProducerAddr
		self.ProducerPort = ProducerPort
		self.ResultCollectorAddr = ResultCollectorAddr
		self.ResultCollectorPort = ResultCollectorPort
		self.ConsumerID = ConsumerID
		self.ResultCollectorCB = ResultCollectorCB
		self.WorkerThread = None
		self.WorkerThreadRunning = False
		self.ZMQContext = zmq.Context()
		self.SpawnSocket = None
		self.LocalProcess = True

		# Create Push Socket if does not exist
		if PushSocket == None:
			PushSocket = self.ZMQContext.socket(zmq.PUSH)
			PushSocket.bind("tcp://"+ProducerAddr+":"+str(ProducerPort))

	def InitLocalCommunication(self):
		global LocalSpawnerSocket
		if LocalSpawnerSocket == None:
			LocalSpawnerSocket = self.ZMQContext.socket(zmq.PAIR)
			LocalSpawnerSocket.bind("tcp://127.0.0.1:5655")

	def InitRemoteCommunication(self, RemoteAddr, RemotePort):
		self.SpawnSocket = self.ZMQContext.socket(zmq.PAIR)
		self.SpawnSocket.connect("tcp://"+RemoteAddr+":"+str(RemotePort))

	def LocalSpawn(self):
		global WorkerThreadStarted
		self.LocalProcess = True
		arguments = [self.ProcessPath, self.ProducerAddr, str(self.ProducerPort), self.ResultCollectorAddr, str(self.ResultCollectorPort), str(self.ConsumerID)]
		subprocess.Popen(["python"] + arguments)

		self.InitLocalCommunication()
		
		if WorkerThreadStarted == False:
			self.WorkerThread = Thread(target=self.WorkReceiveMain, args=())
			self.daemon = True
			self.WorkerThread.start()
			WorkerThreadStarted = True
		

	def RemoteSpawn(self, RemoteAddr, RemotePort = 5665):
		self.LocalProcess = False
		self.InitRemoteCommunication(RemoteAddr, RemotePort)
		
		msg = {"Req": "spawn", 
			"ProcessPath": self.ProcessPath, 
			"ProducerAddr": self.ProducerAddr,
			"ProducerPort": self.ProducerPort,
			"ResultCollectorAddr": self.ResultCollectorAddr,
			"ResultCollectorPort": self.ResultCollectorPort,
			"ConsumerID": self.ConsumerID}
		SpawnSocket.send_json(msg)
		self.WorkerThread = Thread(target=self.WorkReceiveMain, args=())
		self.daemon = True
		self.WorkerThread.start()
		WorkerThreadStarted = True

	def ShutdownConsumer(self):
		global LocalSpawnerSocket
		msg = {"Req": "shutdown", "ConsumerID": self.ConsumerID}
		if LocalSpawnerSocket != None:
			LocalSpawnerSocket.send_json(msg)

		if self.SpawnSocket != None:
			self.SpawnSocket.send_json(msg)
		

	def WorkReceiveMain(self):
		PullSocket = None
		
		# Create Pull Socket if does not exist
		if PullSocket == None:
			PullSocket = self.ZMQContext.socket(zmq.PULL)
			PullSocket.bind("tcp://"+self.ResultCollectorAddr+":"+str(self.ResultCollectorPort))

		self.WorkerThreadRunning = True
		while self.WorkerThreadRunning:
			# Receive data from consumer socket
			processedData = PullSocket.recv_multipart()

			# Get Header information
			hdr = processedData[0].decode()
			hdr = json.loads(hdr)

			# Get Additional Data
			addData = processedData[1].decode()
			addData = json.loads(addData)

			# Get payload
			payload = processedData[2]

			# Forward processed data
			self.ResultCollectorCB(hdr, addData, payload)